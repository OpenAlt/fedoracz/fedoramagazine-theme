��    ?        Y         p     q     x  	   �     �     �     �     �     �     �  	   �                    (     9     N     _     l     q  	   v     �  	   �     �     �     �  �   �     v     �     �     �     �     �     �     �  	   �  
   �  (   �                    &     4     G     U     d     j     q     }  
   �     �     �     �     �     �  
   �     �     �  	   �     �     �  )   �     %	  �  +	  	   �     �     �     �     �     �               ,  	   <     F     L  	   c  "   m     �     �     �     �     �     �     �  	   �  
     
     
     �   (     �       
   "     -     2     :     A     X     p     �  5   �     �     �     �               -     F     `     l     {     �     �     �     �     �     �     �               %  	   ,     6     G  ,   K     x     
                     <          6       *           >       .   ?      5   =       +                   4                 1   ;       #   (             -       !      	      :          7                    &           $         0                    )   '             %       /   2   ,   9   8   3       "                     (Edit) About the attachment Add yours Archive Archives by Categories Archives by Day Archives by Month Archives by Tags Archives by Year Audio URL Author Baskerville Options Category Comment Comments Comments are closed. Continue Reading Contributors Date Edit Edit post Email Error 404 Footer A Footer B Footer C It seems like you have tried to open a page that doesn't exist. It could have been deleted, moved, or it never existed at all. You are welcome to search for what you are looking for with the form below. Last 30 Posts Latest posts Link URL Logo Month Name Newer Comments Newer posts Next post Next post: No results. Try again, would you kindly? Older Comments Older posts Page %s Page %s of %s Pingback Pingbacks Previous post Previous post: Reply Search Search form Search results Show email Sidebar Sticky post Tag Text widget Title To the top Type and press enter Up Video URL Website Year Your email address will not be published. posts Project-Id-Version: Baskerville v1.00
Report-Msgid-Bugs-To: 
POT-Creation-Date: 
PO-Revision-Date: 2016-01-28 01:14+0100
Last-Translator: frantisekz <Zatloukal.Frantisek@gmail.com>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Poedit 1.8.6
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
Language: cs_CZ
X-Poedit-SearchPath-0: .
 (Upravit) O příloze Přidejte svůj Archiv Archivy podle kategorií Denní archivy Měsíční archivy Archivy podle tagů Roční archivy URL zvuku Autor Nastavení Baskerville Kategorie Komentář Komentáře Komentáře Komentáře jsou uzavřeny. Pokračovat ve čtení Spolupracovníci Datum Upravit Upravit příspěvek Email Chyba 404 Patička A Patička B Patička C Zdá se, že jste se pokusili otevřít stránku, která neexistuje. Mohla být smazána, přesunuta a nebo nikdy nemusela existovat. Můžete tuto stránku zkusit vyhledat pomocí pole níže. Posledních 30 příspěvků Nejnovější příspěvky URL odkazu Logo Měsíc Jméno Novější komentáře Novější příspěvky Další příspěvek Další příspěvek: Žádné výsledky. Můžete to prosím zkusit znova? Starší komentáře Starší příspěvky Stránka %s Stránka %s z %s Pingback Pingbacks  Předchozí příspěvek Předchozí příspěvek: Odpovědět Vyhledávání Vyhledávací formulář Výsledky vyhledávání Zobrazit email Postranní panel Připíchnutý příspěvek Tag Textový widget Název Nahoru Pište a stiskněte enter Nahoru URL videa Webová stránka Rok Vaše e-mailová adresa nebude zveřejněna. příspěvky 